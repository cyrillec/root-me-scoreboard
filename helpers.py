import requests
import re

users={}

def highlight():
  global users
  for u in users:
    for problem in users[u]["validated"]:
      other_user_validation = [ x for v in users if v!= u for x in users[v]["validated"] ]
      if problem not in other_user_validation:
        users[u]["highlight"].append(problem)

def add_user(username, score, rank, name, validated, pending, highlight=[]):
  if username not in users:
    users[username]={}
  users[username]["score"]=score
  users[username]["rank"]=rank
  users[username]["name"]=name
  users[username]["validated"]=validated
  users[username]["pending"]=pending
  users[username]["highlight"]=[] # To fill with highlight() later

def request_and_parse_user(username):
  r=requests.get("https://www.root-me.org/%s"%username, params={"inc":"score"})
  if r.status_code==200:
    score=re.search("(\d+)&nbsp;Points&nbsp;", r.text)
    score=int(score.group(1))
    validated = []
    pending = []
    for m in re.findall(r'\<a class=" (vert|rouge)".*?(x|o)&nbsp;(.*?)<', r.text):
      (color, status, problem) = m
      if 'o'==status:
         validated.append(problem)
      else:
         pending.append(problem)
    return score, validated, pending

def get_user_list():
  global users
  return users

def get_user(username):
  global users
  if username in users:
    return users[username]
  else:
    return None

def reset_users():
  global users
  users={}
