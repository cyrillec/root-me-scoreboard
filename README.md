# My RootMe Scoreboard

## INTRODUCTION

This small webapp is a scoreboard for [root-me.org](https://www.root-me.org).

## INSTALL
This application requires python3

```
git clone http://gitlab.com/cyrillec/root-me-scoreboard.git
pip3 install -r requirements.txt
cp users.json.sample users.json
python3 app.py
```

## USERS Format
Do not hesitate to edit `users.json` to remove sample users and add your freinds. The app will synchronize with root-me to get their stats.

Format:

```
{
  "<username>": "<firstname and lastname>"
}
```

## BROWSE
By default it bind to `any` with port `5000`. Browse to `http://localhost:5000`.

![My RootMe Scoreboard screenshot](screenshot/root-me-scoreboard.png)


## CREDITS
This webapp is largely inspired from [https://github.com/gteissier/rootme-score](https://github.com/gteissier/rootme-score) command lines.

Cheers!

Cyrillec
