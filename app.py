from flask import Flask, render_template, redirect
from flask_bootstrap import Bootstrap
from helpers import reset_users, get_user, get_user_list, highlight, add_user, request_and_parse_user
import json


app = Flask(__name__)
Bootstrap(app)

@app.route("/")
def index():
  users=get_user_list()
  return render_template("index.html", users=users)

@app.route("/user/<username>")
def user(username):
  profile=get_user(username)
  if not user:
    return redirect("/")
  return render_template("user.html", username=username ,profile=profile)

@app.route("/refresh")
def refresh():
  reset_users()
  with open("users.json","r") as f:
    #_users file reference
    _users=json.loads(f.read())
  for u in _users:
    users=get_user_list()
    (score, validated, pending) = request_and_parse_user(u)
    if score:
      # How many user before current user?
      rank = 1 + len([u for u in users if users[u]['score'] >= score])
      # Increase rank for users below current user
      for x in users:
        if users[x]["score"] < score:
          users[x]["rank"]+=1
      add_user(username=u, score=score, rank=rank, name=_users[u]["name"], validated=validated, pending=pending)
  highlight()
  return redirect("/") 

if __name__ == "__main__":
    refresh()
    app.run(host="0.0.0.0")

